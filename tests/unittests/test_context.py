from pytest import raises
from modules.context import ApplicationContext


def test_application_context_headers():
    context = ApplicationContext()
    assert context.headers == dict()

    context.headers = {1: 2}
    assert context.headers == {1: 2}

    del context.headers
    assert context.headers == dict()

    with raises(ValueError):
        context.headers = 'abc'
    with raises(TypeError):
        context.headers = 1
    with raises(TypeError):
        context.headers = (1, 2, 3)
    with raises(TypeError):
        context.headers = [1, 2, 4]
    with raises(TypeError):
        context.headers = None


def test_set_headers_from_console():
    context = ApplicationContext()
    assert context.headers == dict()
    headers: tuple = (r'Accept:application\json',
                      r'Test : this_way',
                      r'Another: like_this',
                      r'And :like_this',)
    context.set_from_console(web_headers=headers)
    assert context.headers['Accept'] == r'application\json'
    assert context.headers['Test'] == r'this_way'
    assert context.headers['Another'] == r'like_this'
    assert context.headers['And'] == r'like_this'
    with raises(ValueError):
        context.set_from_console(web_headers=('abc',))
    with raises(TypeError):
        context.set_from_console(web_headers=1)
    with raises(AttributeError):
        context.set_from_console(web_headers=(1, 2, 3))
    with raises(AttributeError):
        context.set_from_console(web_headers=[1, 2, 4])

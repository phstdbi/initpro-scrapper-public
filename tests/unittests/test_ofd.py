import ofd


def test_main():
    with open(ofd.__file__, 'r') as main:
        exec(main.read())


def test_argparse():
    ofd.argparse(__name__, ['-f', 'some/file/dir/file.txt'])

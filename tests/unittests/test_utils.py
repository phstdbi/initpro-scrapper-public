from pytest import raises
from modules.utils import verify_type


def test_verify_type():
    some_dict = dict()
    some_list = list()
    assert some_dict == verify_type(some_dict, dict, allowed_none=False, convert_none=False)
    assert verify_type(None, dict, allowed_none=True, convert_none=False) is None
    assert some_dict == verify_type(None, dict, allowed_none=True, convert_none=True)
    with raises(TypeError):
        verify_type(some_list, dict, allowed_none=False, convert_none=False)

"""
ToDo: Write docstring
"""
from requests import Session


DEFAULT_TEST_API = False


class API(object):

    def __init__(self,
                 test: bool = False,
                 protocol: str = 'https',
                 domain: str = 'ofd-initpro.ru',
                 api_path: str = '/lk/api/v1/'):
        self._test = test
        self.protocol = protocol
        self.domain = domain
        self.api_path = api_path

    @property
    def test(self):
        return 'test.' if self._test else str()

    @test.setter
    def test(self, is_test: bool):
        self._test = is_test

    def auth(self, username, password):
        print(f'{self.protocol}://{self.test}{self.domain}{self.api_path}access/auth?id={username}&psw={password}')
        return f'{self.protocol}://{self.test}{self.domain}{self.api_path}access/auth?id={username}&psw={password}'

    def cashboxes(self, token):
        return f'{self.protocol}://{self.test}{self.domain}{self.api_path}cashboxes/list?api_key={token}'

    def receipts(self, token, cashbox, start, date_from, date_to):
        return f'{self.protocol}://{self.test}{self.domain}{self.api_path}' \
               f'cashboxes/{cashbox}/documents?api_key={token}&start={start}' \
               f'&onpage=10&documentType=receipt&dateFrom={date_from}&dateTo={date_to}'

    def items(self, token, cashbox, document, fs_id):
        return f'{self.protocol}://{self.test}{self.domain}{self.api_path}' \
               f'cashboxes/check/{cashbox}/{document}/{fs_id}?api_key={token}'


class InitproAPI(Session):

    def __init__(self, application_context):
        super().__init__()
        if application_context.headers:
            self.headers.update(application_context.headers)
        self.context = application_context
        self.token = None

    def set_token(self):
        response = self.get(self.context.api.auth(
            username=self.context.ofd_user['username'],
            password=self.context.ofd_user['password']))
        js = response.json()
        self.token = js['data']['api_key']

    def get_cashboxes(self):
        response = self.get(self.context.api.cashboxes(token=self.token))
        js = response.json()
        if not js['success']:
            self.set_token()
            return self.get_cashboxes()
        return js['data']

    def get_receipts(self, cashbox, date_from, date_to, start):
        response = self.get(self.context.api.receipts(
            cashbox=cashbox,
            token=self.token,
            start=start,
            date_from=date_from,
            date_to=date_to
        ))
        js = response.json()
        if not js['success']:
            self.set_token()
            return self.get_receipts(cashbox, date_from, date_to, start)
        return js['data']

    def get_items(self, cashbox, fs_id, document):
        response = self.get(self.context.api.items(
            cashbox=cashbox,
            token=self.token,
            document=document,
            fs_id=fs_id
        ))
        js = response.json()
        if not js['success']:
            self.set_token()
            return self.get_items(cashbox, fs_id, document)
        return js['data']


if __name__ == '__main__':
    pass

"""
ToDo: Write docstring
"""
import datetime


def verify_type(obj, allowed_type, allowed_none: bool = False, convert_none: bool = False, name: str = None):
    """
    Checks if given object is instance of certain type.

    Args:
        obj: object to inspect;
        allowed_type: type for inspection;
        allowed_none: boolean option if None objects should pass the inspection;
        convert_none: boolean option if None object should be converted to an empty object of a given type;
        name: name of the variable for verbose error output;

    Returns:
        Given object if it is an instance of given type;
        OR None if it was passed as an object and allowed_none argument is True:
        OR an empty object of the given type if None was passed as an object and allowed_none and convert_none arguments
            are both True;

    Raises:
        A TypeError if none of the return conditions were passed;
    """
    if isinstance(obj, allowed_type):
        return obj
    elif obj is None and allowed_none:
        if convert_none:
            obj: allowed_type = allowed_type()
        return obj
    else:
        or_none: str = f" or {type(None)}" if allowed_none else str()
        name = str() if name is None else name + " "
        raise TypeError(f"Variable {name}expected to be {allowed_type}{or_none}, not {type(obj)}")


def datetime_from_string(string):
    date, time = string.split(' ')
    day, month, year = date. split('.')
    hour, minute = time.split(':')
    year, month, day, hour, minute = int(year), int(month), int(day), int(hour), int(minute)
    return datetime.datetime(year, month, day, hour=hour, minute=minute)

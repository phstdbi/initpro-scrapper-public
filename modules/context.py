"""
ToDo: Write docstring
"""
from configparser import ConfigParser
from .utils import verify_type
from .web import API, DEFAULT_TEST_API


class ApplicationContext(object):
    """
    ToDo: Docstring
    """

    def __init__(self,
                 headers: dict = None,
                 ofd_user: dict = None):
        self._headers: dict = verify_type(headers, dict, True, True, "headers")
        self._api: API = API(test=DEFAULT_TEST_API)
        self.ofd_user: dict = verify_type(ofd_user, dict, True, True, 'ofd_user')
        self.engine = None
        self.session = None
        self.db_settings = None

    @property
    def headers(self) -> dict:
        return self._headers

    @headers.setter
    def headers(self, headers: dict):
        self._headers.update(headers)

    @headers.deleter
    def headers(self):
        self._headers: dict = dict()

    @property
    def api(self):
        return self._api

    @api.setter
    def api(self, test_api=DEFAULT_TEST_API):
        self._api = API(test=test_api)

    @api.deleter
    def api(self):
        self._api = API(test=DEFAULT_TEST_API)

    def set_from_env(self, env_prefix):
        pass

    def set_from_file(self, context_file):
        parser = ConfigParser()
        parser.read(context_file)
        self.headers = dict(parser['WEB SESSION HEADERS'])
        self.ofd_user = dict(parser['WEB USER'])
        self.db_settings = dict(parser['DATABASE'])

    def set_from_console(self, test_api: bool = DEFAULT_TEST_API):
        if test_api:
            self.api = True

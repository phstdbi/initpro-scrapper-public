from sqlalchemy import create_engine as make_engine
from sqlalchemy.ext.declarative import as_declarative
from sqlalchemy.engine.url import URL
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey
from sqlalchemy.orm import sessionmaker
from .context import ApplicationContext


@as_declarative()
class Base(object):
    id = Column(Integer, primary_key=True)


class Cashbox(Base):
    __tablename__ = 'cashboxes'
    name = Column(String(250))


class Receipt(Base):
    __tablename__ = 'receipts'
    id = Column(String(250), primary_key=True)
    fs_id = Column(Integer)
    datetime = Column(DateTime)
    icon = Column(Integer)
    type = Column(Integer)
    receipt_type_title = Column(String(250))
    sum_type_title = Column(String(250))
    number = Column(Integer)
    summ_cash = Column(Integer)
    summ_ecash = Column(Integer)
    summ_any = Column(Integer)
    summ = Column(Integer)
    title = Column(String(250))
    turn = Column(Integer)
    cashbox = Column(Integer, ForeignKey('cashboxes.id'))


class Item(Base):
    __tablename__ = 'items'
    method_of_calculation = Column(Integer)
    indication_of_subject = Column(Integer)
    name = Column(String(250))
    item_code = Column(String(250))
    origin_country_code = Column(String(250))
    price = Column(Integer)
    quantity = Column(Integer)
    nds_rate = Column(Integer)
    sum_nds_per_item = Column(Integer)
    summ = Column(Integer)
    receipt = Column(String(250), ForeignKey('receipts.id'))


def create_engine(context: ApplicationContext):
    db = context.db_settings
    url = URL(drivername=db['drivername'],
              username=db['username'],
              password=db['password'],
              host=db['host'],
              port=db['port'],
              database=db['database'])
    return make_engine(url, echo=True)


def create_session(engine):
    session_class = sessionmaker(bind=engine)
    return session_class()


def create_all(context: ApplicationContext):
    engine = create_engine(context)
    Base.metadata.create_all(engine)
    session = create_session(engine)
    return engine, session


def get_cashbox_by_id(context, id):
    session = context.session
    return session.query(Cashbox).filter_by(id=id).first()


def create_cashbox(context, id, name):
    session = context.session
    cashbox = Cashbox(id=id, name=name)
    session.add(cashbox)
    session.commit()


def get_receipt_by_id(context, id):
    session = context.session
    return session.query(Receipt).filter_by(id=id).first()


def create_receipt(context, id, fs_id, datetime, icon, type, receipt_type_title, sum_type_title,
                   number, summ_cash, summ_ecash, summ_any, summ, title, turn, cashbox):
    receipt = Receipt(id=id,
                      fs_id=fs_id,
                      datetime=datetime,
                      icon=icon,
                      type=type,
                      receipt_type_title=receipt_type_title,
                      sum_type_title=sum_type_title,
                      number=number,
                      summ_cash=summ_cash,
                      summ_ecash=summ_ecash,
                      summ_any=summ_any,
                      summ=summ,
                      title=title,
                      turn=turn,
                      cashbox=cashbox)
    session = context.session
    session.add(receipt)
    session.commit()


def create_item(context, method_of_calculation, indication_of_subject, name, item_code,
                origin_country_code, price, quantity, nds_rate, sum_nds_per_item, summ, receipt):
    item = Item(method_of_calculation=method_of_calculation,
                indication_of_subject=indication_of_subject,
                name=name,
                item_code=item_code,
                origin_country_code=origin_country_code,
                price=price,
                quantity=quantity,
                nds_rate=nds_rate,
                sum_nds_per_item=sum_nds_per_item,
                summ=summ,
                receipt=receipt)
    session = context.session
    session.add(item)
    session.commit()

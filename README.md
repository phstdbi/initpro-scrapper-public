# README #

ofd. py - скрипт для скраппинга данных эектронных чеков с сайта ОФД InitPro

### Установка ###

Для установки, в среде с установленным пакетом git, выполнить

**git clone https://GrigoryGil@bitbucket.org/phstdbi/initpro-scrapper-public.git**

### Конфигурация ###

#### Конфигурация скрипта ####
В корневой директории скрипта находится конфигурационнный файл, по умолчанию *ofd.cfg*
Имя файла передается параметром скрипту и может быть изменено.

Конфигурационный файл состоит из секций, начинающихся с названия секции в квадратных скобках
**[ПРИМЕР НАЧАЛА СЕКЦИИ]** и завершающихся началом следующей секции или концом файла, а также
конфигурационных записей вида **ключ = значение**.
Порядок секций и записей внутри секции не важен.

##### [WEB SESSION HEADERS] #####
Все пары ключей, значений соответствуют парам ключ, значение веб-заголовка, при отправке запросов к API InitPro.

##### [WEB USER] #####
Обязательные параметры:

* username, password - учетные данные личного кабинета InitPro, используемые для входа.

##### [DATABASE] #####
Обязательные параметры:

* drivername - тип используемой СУБД. В настоящий момент, реализована поддержка только базы MySQL (Зачение строки базы - **mysql+pymysql**);
* username - имя пользователя базы данных;
* password - пароль пользователя базы данных;
* host - расположение базы данных;
* port - порт для соединения с базой данных;
* database - имя базы данных.

#### Конфигурация docker compose ####

В корневой директории скрипта находится файл *docker-compose.yml*, служащий для конфигурации
оркестра контейнеров.

##### Конфигурация контейнера базы данных #####

    mysql_db: # Имя базы данных в записях hosts
        image: mysql:5.7 # Имя образа базы данных
        environment:
            MYSQL_ROOT_PASSWORD: password # Пароль администраторской учетной записи
            MYSQL_DATABASE: db # Имя рабочей базы данных
            MYSQL_USER: user # Имя пользователя базы данных
            MYSQL_PASSWORD: password # Пароль пользователя базы данных
        volumes:
            - ./mysql:/var/lib/mysql # Расположение монтируемой области хранения данных
        ports:
            - 3307:3306 # Порт базы данных
            
##### Конфигурация контейнера скрипта #####

    python_app: # Скрипта в записях hosts
        command:
            python ofd.py -w 10 --test_api --context_file ofd.cfg run --date_from 16.03.2020 --date_to 19.03.2020 # Команда запуска скрипта в контейнере

### Использование скрипта ###

Скрипт запускается командой, имеющей следующую структуру:

**python ofd.py [OPTIONS] COMMAND [ARGS]**

Options:

* -w, --wait - Задержка в секундах, от момента запуска команды, до начала исполнения[1];
* -f, --context_file - Расположение конфигурационного файла скрипта;
* -t, --test_api - Флаг указывающий использовать API тестового портала InitPro;
* --help - Вызов подсказки.

Commands:

* run - Основная команда выполнения скрипта. Выполняет загрузку данных из ОФД с даты начала
  по дату окончания.

run args:

* --date_from - стартовая дата в формате ДД.ММ.ГГГГ;
* --date_to - конечная дата в формате ДД.ММ.ГГГГ.

### Запуск оркестра контейнеров ###

Для запуска всего оркестра контейнеров, использовать команду:

**docker-compose up**

При успешном выполнении, контейнер **python_app** завершится с кодом 0.
Для повторного запуска контейнера скрипта(при запущенном)[2], использовать команду:

**docker-compose up python_app**

Для запуска контейнера базы данных, использовать команду:

**docker-compose up mysql_db**

Для остановки всех контейнеров, использовать команду:

**docker-compose down**

**[1]** Docker compose запускает контейнеры одновременно, поэтому скриптможет начать выполняться
до того, как база данных начнет принимать соединения и скрипт завершится с ошибкой.

**[2]** Для поддержания базы данных в состоянии T-1, следует создать запись запуска команды
запуска контейнера **python_app** с триггером по расписанию в службе cron.
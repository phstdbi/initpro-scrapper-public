"""
ToDo: Write docstring
"""

import click
from time import sleep

from modules.context import ApplicationContext
from modules.database import create_all
from modules.database import get_cashbox_by_id, create_cashbox
from modules.database import get_receipt_by_id, create_receipt
from modules.database import create_item
from modules.web import InitproAPI
from modules.utils import datetime_from_string

application_context: ApplicationContext = ApplicationContext()


@click.group()
@click.option('-w', '--wait', type=int, default=0)
@click.option('-f', '--context_file', type=str)
@click.option('-e', '--context_env_prefix', type=str)
@click.option('-t', '--test_api', is_flag=True)
def argparse(wait: int,
             context_file: str,
             context_env_prefix: str,
             test_api: bool):
    sleep(wait)
    if context_env_prefix:
        application_context.set_from_env(context_env_prefix)
    if context_file:
        application_context.set_from_file(context_file)
    application_context.set_from_console(test_api=test_api)


@argparse.command()
@click.option('--date_from', type=str, required=True)
@click.option('--date_to', type=str, required=True)
def run(date_from, date_to):
    application_context.engine, application_context.session = create_all(application_context)
    api = InitproAPI(application_context)
    api.set_token()
    cashboxes = api.get_cashboxes()
    for cashbox in cashboxes:
        if not get_cashbox_by_id(application_context, cashbox['value']):
            create_cashbox(application_context, cashbox['value'], cashbox['title'])
        count = api.get_receipts(cashbox['value'], date_from, date_to, 1)['count']
        receipts = list()
        for start in range(1, count + 1, 10):
            resp = api.get_receipts(cashbox['value'], date_from, date_to, start)
            for item in resp['rows']:
                receipts.append(item)
        for receipt in receipts:
            if not get_receipt_by_id(application_context, receipt['id']):
                create_receipt(application_context,
                               receipt['id'],
                               receipt['fs_id'],
                               datetime_from_string(receipt['datetime']),
                               receipt['icon'],
                               receipt['type'],
                               receipt['receiptTypeTitle'],
                               receipt['sumTypeTitle'],
                               receipt['number'],
                               receipt['summ_cash'],
                               receipt['summ_ecash'],
                               receipt['summ_any'],
                               receipt['summ'],
                               receipt['title'],
                               receipt['turn'],
                               cashbox['value'])
                document_id = receipt['id'][len(str(receipt['fs_id'])):]
                items = api.get_items(cashbox['value'], receipt['fs_id'], document_id)['items']
                for item in items:
                    create_item(application_context,
                                method_of_calculation=item['methodOfCalculation'],
                                indication_of_subject=item['indicationOfSubject'],
                                name=item['name'],
                                item_code=item['itemCode'] if 'itemCode' in item else str(),
                                origin_country_code=item['originCountryCode'],
                                price=item['price'],
                                quantity=item['quantity'],
                                nds_rate=item['ndsRate'],
                                sum_nds_per_item=item['sumNdsPerItem'],
                                summ=item['sum'],
                                receipt=receipt['id'])


if __name__ == '__main__':
    argparse()
